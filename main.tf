terraform {
  required_version = ">=0.12.0"
  backend "s3" {
    bucket = "hello-kubernetes-abn"
    key    = "terraform.tfstate"
    region = "eu-west-1"
  }
}

data "aws_eks_cluster" "cluster" {
  name = module.eks.cluster_id
}

data "aws_eks_cluster_auth" "cluster" {
  name = module.eks.cluster_id
}

provider "kubernetes" {
  host                   = data.aws_eks_cluster.cluster.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority.0.data)
  token                  = data.aws_eks_cluster_auth.cluster.token
  load_config_file       = false
  version                = "~> 1.12"
}

provider "aws" {
  version = ">= 2.28.1"
  region  = var.aws["region"]
  profile = var.aws["profile"]
}


locals {
  cidr = var.networking["cidr"]
  azs = [
    for az in var.networking["azs"]:
      join("", [var.aws["region"],az])
  ]
  private_subnets = [
    for netnum in var.networking["subnets"]["private"]["netnums"]:
      cidrsubnet(local.cidr, 4, netnum)
  ]
  public_subnets = [
    for netnum in var.networking["subnets"]["public"]["netnums"]:
      cidrsubnet(local.cidr, 4, netnum)
  ]
  cluster_name    = "${var.aws["env"]}-${var.eks["name"]}"
}

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "2.51.0"

  name            = "${var.aws["env"]}-eks"
  cidr            = local.cidr
  azs             = local.azs
  private_subnets = local.private_subnets
  public_subnets  = local.public_subnets
  enable_nat_gateway   = true
  single_nat_gateway   = true
  enable_dns_hostnames = true
  tags            = var.standard_tags
  public_subnet_tags = {
    "kubernetes.io/cluster/${local.cluster_name}" = "shared"
    "kubernetes.io/role/elb"                      = "1"
  }

  private_subnet_tags = {
    "kubernetes.io/cluster/${local.cluster_name}" = "shared"
    "kubernetes.io/role/internal-elb"             = "1"
  }
}

module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "12.2.0"

  cluster_name    = local.cluster_name
  cluster_version = "1.17"
  subnets         = module.vpc.private_subnets

  tags = var.standard_tags

  vpc_id = module.vpc.vpc_id

  worker_groups = var.eks["worker_groups"]
}
