resource "kubernetes_deployment" "hello" {
  metadata {
    name = var.app["name"]
  }
  spec {
    replicas = 3
    selector {
      match_labels = {
        app = var.app["name"]
      }
    }
    template {
      metadata {
        labels = {
          app = var.app["name"]
        }
      }
      spec {
        container {
          name  = var.app["name"]
          image = "${var.app["image"]}:${var.app["version"]}"
          port {
            container_port = 8080
          }
          resources {
            limits {
              cpu    = var.app["cpu_limit"]
              memory = var.app["memory_limit"]
            }
            requests {
              cpu    = var.app["cpu_request"]
              memory = var.app["memory_request"]
            }
          }
          liveness_probe {
            tcp_socket {
              port  = 8080
            }
            initial_delay_seconds = 2
            period_seconds = 2
          }
          readiness_probe {
            http_get {
              path  = "/"
              port  = 8080
            }
            initial_delay_seconds = 2
            period_seconds = 2
          }
        }
      }
    }
  }
  depends_on = [module.eks]
}

resource "kubernetes_service" "hello" {
  metadata {
    name = var.app["name"]
  }
  spec {
    type = "LoadBalancer"
    port {
      port = 80
      target_port = 8080
    }
    selector = {
      app = var.app["name"]
    }
  }
  depends_on = [kubernetes_deployment.hello]
}

output "hello" {
  value = kubernetes_service.hello.load_balancer_ingress[0].hostname
}
