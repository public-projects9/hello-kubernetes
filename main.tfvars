aws = {
  "profile"        = "default"
  "region"         = "eu-west-1"
  "env"            = "dev"
}

networking = {
  cidr = "10.0.0.0/16"
  azs = ["a","b","c"]
  subnets = {
    public = {
      netnums = [1,2,3]
    },
    private = {
      netnums = [4,5,6]
    }
  }
}

standard_tags = {
  Github      = "terraform-base-abn"
  Environment = "development"
  Terraform   = "true"
}


eks = {
  name            = "app-cluster"
  cluster_version = "1.17"
  worker_groups   = [
    {
      instance_type = "t3.medium"
      asg_min_size  = 3
      asg_max_size  = 3
      asg_desired_capacity = 3
    }
  ]
}

app = {
  name = "hello-kubernetes"
  version = "1.7"
  image = "paulbouwer/hello-kubernetes"
  cpu_limit = "0.5"
  memory_limit = "512Mi"
  cpu_request = "250m"
  memory_request = "50Mi"
}
