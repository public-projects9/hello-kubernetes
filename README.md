# Purpose
Hello World App

## Tools used
* Terraform  (version 0.13.3)
* EKS (version 1.17)

## Requirements
1. AWS account credentials as environment variables for the project in Gitlab
2. Create S3 bucket for the Terraform state (defined in main.tfvars)

## Running Terraform pipeline to build the Environment

* Everything runs in Gitlab.
* You make Terraform changes to the 'master' branch and your updates will be applied automatically
* Terraform guarantees that the Kubernetes deployments are successful & canary test done afterwards to ensure connection to ELB

## What is created?
* VPC with Private + Public Subnets, Routing Tables, Security Groups
* EKS (Cluster, Auto-scaling group for App)
* Kubernetes App deployment and LB service
