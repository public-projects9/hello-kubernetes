variable "aws" {
  type = map(string)
}


variable "networking" {
  type    = any
  default = []
}


variable "standard_tags" {
  type    = map(string)
}

variable "eks" {
  type = any
}

variable "app" {
  type    = map(string)
}
